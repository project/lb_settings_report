<?php

namespace Drupal\Tests\lb_settings_report\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests layout builder settings report.
 *
 * @group lb_settings_report
 */
class LayoutBuilderSettingsReportTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public $profile = 'testing';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Authorized User.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $authorizedUser;

  /**
   * Unauthorized User.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $unauthorizedUser;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'lb_settings_report',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->authorizedUser = $this->drupalCreateUser([
      'view layout builder settings report',
    ]);

    $this->unauthorizedUser = $this->drupalCreateUser([]);
  }

  /**
   * Test that our report is only accessible with the appropriate permission.
   */
  public function testPermissions() {
    // Check unauthorized user.
    $assert = $this->assertSession();
    $this->drupalGet('admin/reports/layout-builder-settings');
    $assert->statusCodeEquals(403);

    // Check authorized user.
    $this->drupalLogin($this->authorizedUser);
    $this->drupalGet('admin/reports/layout-builder-settings');
    $assert->statusCodeEquals(200);
  }

}
