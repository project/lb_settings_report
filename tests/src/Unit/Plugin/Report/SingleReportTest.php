<?php

namespace Drupal\Tests\lb_settings_report\Unit\Plugin\Report;

use Drupal\lb_settings_report\Plugin\Subject\SubjectInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\Tests\lb_settings_report\Unit\MockTestTrait;

/**
 * @coversDefaultClass \Drupal\lb_settings_report\Plugin\Report\SingleReport
 *
 * @group lb_settings_report
 */
class SingleReportTest extends UnitTestCase {

  use MockTestTrait;

  /**
   * Tests SingleReport::encode().
   */
  public function testEncodePerformsYamlEncoding() {
    $report = $this->getSingleReportMock();
    $original = [
      'a' => 'A',
      'b' => ['x', 2, 3.5],
      'c' => NULL,
      'd' => FALSE,
    ];
    $expected = implode(PHP_EOL, [
      'a: A',
      'b:',
      '  - x',
      '  - 2',
      '  - 3.5',
      'c: null',
      'd: false',
    ]);
    $this->assertEquals($expected, $report->encode($original));
  }

  /**
   * Tests SingleReport::getSubjects().
   */
  public function testGetSubjectsCreatesOneSubject() {
    $report = $this->getSingleReportMock(1);

    $expected_keys = [
      'lb_settings_report__single__0',
    ];
    $actual = $report->getSubjects();
    $this->assertEquals($expected_keys, array_keys($actual));
    foreach ($expected_keys as $expected_key) {
      $this->assertInstanceOf(SubjectInterface::CLASS, $actual[$expected_key]);
      $this->assertEquals('Layout Builder Subject', $actual[$expected_key]->getNumberedLabel());
    }
  }

  /**
   * Tests SingleReport::generateReport().
   */
  public function testGenerateReportAssemblesYaml() {
    $report = $this->getSingleReportMock(1);
    $values = [
      'lb_settings_report__single__0' => [],
    ];
    /* @see \Drupal\Tests\lb_settings_report\Unit\MockTestTrait::getSubjectManagerMock(). */
    $expected = '<pre class="lbSettingsReport__preformatted">' .
      implode(PHP_EOL, [
        "- 'Mock Report'",
        "- 'Subject 0'",
        "- 'End of Report'",
      ]) . '</pre>';
    $this->assertEquals($expected, $report->generateReport($values));
  }

}
