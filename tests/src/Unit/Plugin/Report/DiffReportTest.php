<?php

namespace Drupal\Tests\lb_settings_report\Unit\Plugin\Report;

use Drupal\lb_settings_report\Plugin\Subject\SubjectInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\Tests\lb_settings_report\Unit\MockTestTrait;

/**
 * @coversDefaultClass \Drupal\lb_settings_report\Plugin\Report\DiffReport
 *
 * @group lb_settings_report
 */
class DiffReportTest extends UnitTestCase {

  use MockTestTrait;

  /**
   * Tests DiffReport::encode().
   */
  public function testEncodePerformsYamlEncoding() {
    $report = $this->getDiffReportMock();
    $original = [
      'a' => 'A',
      'b' => ['x', 2, 3.5],
      'c' => NULL,
      'd' => FALSE,
    ];
    $expected = implode(PHP_EOL, [
      'a: A',
      'b:',
      '  - x',
      '  - 2',
      '  - 3.5',
      'c: null',
      'd: false',
    ]);
    $this->assertEquals($expected, $report->encode($original));
  }

  /**
   * Tests DiffReport::getSubjects().
   */
  public function testGetSubjectsCreatesTwoSubjects() {
    $report = $this->getDiffReportMock(2);

    $expected_keys = [
      'lb_settings_report__diff__0',
      'lb_settings_report__diff__1',
    ];
    $actual = $report->getSubjects();
    $this->assertEquals($expected_keys, array_keys($actual));
    $ct = 1;
    foreach ($expected_keys as $expected_key) {
      $label_suffix = $ct > 1 ? " {$ct}" : '';
      $this->assertInstanceOf(SubjectInterface::CLASS, $actual[$expected_key]);
      $this->assertEquals("Layout Builder Subject{$label_suffix}", $actual[$expected_key]->getNumberedLabel());
      $ct++;
    }
  }

  /**
   * Tests DiffReport::getDiff().
   */
  public function testGetDiffEvaluatesCorrectlyForTwoSubjects() {
    $report = $this->getDiffReportMock(2);
    $values = [
      'lb_settings_report__diff__0' => [],
      'lb_settings_report__diff__1' => [],
    ];

    $diff_edits = $report->getDiff($values)->getEdits();
    /* @see \Drupal\Tests\lb_settings_report\Unit\MockTestTrait::getSubjectManagerMock(). */
    $expected_edit_list = [
      [
        'type' => 'copy',
        'orig' => ["- 'Mock Report'"],
        'closing' => ["- 'Mock Report'"],
      ],
      [
        'type' => 'change',
        'orig' => ["- 'Subject 0'"],
        'closing' => ["- 'Subject 1'"],
      ],
      [
        'type' => 'copy',
        'orig' => ["- 'End of Report'"],
        'closing' => ["- 'End of Report'"],
      ],
    ];
    $this->assertEquals(count($expected_edit_list), count($diff_edits));
    foreach ($diff_edits as $idx => $diff_edit) {
      $expected = $expected_edit_list[$idx];
      $this->assertEquals($expected['type'], $diff_edit->type);
      $this->assertEquals($expected['orig'], $diff_edit->orig);
      $this->assertEquals($expected['closing'], $diff_edit->closing);
    }
  }

  /**
   * Tests DiffReport::generateReport().
   */
  public function testGenerateReportAssemblesDiffTable() {
    $report = $this->getDiffReportMock(2);
    $values = [
      'lb_settings_report__diff__0' => [],
      'lb_settings_report__diff__1' => [],
    ];
    $expected = json_encode([
      '#title' => 'Differential Report',
      '#attached' => ['library' => ['system/diff']],
      'diff' => [
        '#type' => 'table',
        '#attributes' => [
          'class' => [
            'diff',
            'lbSettingsReport__table',
            'lbSettingsReport__diffTable',
          ],
        ],
        '#header' => [
          ['data' => 'Subject 1', 'colspan' => '2'],
          ['data' => 'Subject 2', 'colspan' => '2'],
        ],
        '#rows' => 'Diff Results Go Here',
      ],
    ]);
    $this->assertEquals($expected, $report->generateReport($values));
  }

}
