<?php

namespace Drupal\Tests\lb_settings_report\Unit\Plugin\Report;

use Drupal\lb_settings_report\Plugin\Subject\SubjectInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\Tests\lb_settings_report\Unit\MockTestTrait;

/**
 * @coversDefaultClass \Drupal\lb_settings_report\Plugin\Report\OverrideReport
 *
 * @group lb_settings_report
 */
class OverrideReportTest extends UnitTestCase {

  use MockTestTrait;

  /**
   * Tests OverrideReport::encode().
   */
  public function testEncodePerformsYamlEncoding() {
    $report = $this->getOverrideReportMock();
    $original = [
      'a' => 'A',
      'b' => ['x', 2, 3.5],
      'c' => NULL,
      'd' => FALSE,
    ];
    $expected = implode(PHP_EOL, [
      'a: A',
      'b:',
      '  - x',
      '  - 2',
      '  - 3.5',
      'c: null',
      'd: false',
    ]);
    $this->assertEquals($expected, $report->encode($original));
  }

  /**
   * Tests OverrideReport::getSubjects().
   */
  public function testGetSubjectsCreatesOneSubject() {
    $report = $this->getOverrideReportMock(1);

    $expected_keys = [
      'lb_settings_report__override__0',
    ];
    $actual = $report->getSubjects();
    $this->assertEquals($expected_keys, array_keys($actual));
    $ct = 1;
    foreach ($expected_keys as $expected_key) {
      $label_suffix = $ct > 1 ? " {$ct}" : '';
      $this->assertInstanceOf(SubjectInterface::CLASS, $actual[$expected_key]);
      $this->assertEquals("Layout Builder Subject{$label_suffix}", $actual[$expected_key]->getNumberedLabel());
      $ct++;
    }
  }

  /**
   * Tests OverrideReport::generateReport().
   */
  public function testGenerateReportBuildsTable() {
    $config = [];
    $mock_report = [];
    $report = $this->getOverrideReportMock(1);
    $values = [
      'lb_settings_report__override__0' => [],
    ];
    $expected_summary = json_encode([
      '#theme' => 'lb_settings_report_override_summary',
      '#bundle' => 'block_content',
      '#id' => '12345',
      '#language' => 'Spanish',
      '#published' => 'Published',
      '#title' => 'Hola mundo 0',
    ]);
    $expected_sections = json_encode([
      '#theme' => 'lb_settings_report_override_sections',
      '#sections' => ['Section 1', 'Section 2', 'Section 3'],
    ]);
    $expected = json_encode([
      '#title' => 'Override Report',
      'overrides' => [
        '#type' => 'table',
        '#attributes' => [
          'class' => [
            'lbSettingsReport__table',
            'lbSettingsReport__overrideTable',
          ],
        ],
        '#header' => [
          ['data' => ''],
          ['data' => 'Entity Information'],
          ['data' => 'Section Information'],
        ],
        '#rows' => [
          [
            ['data' => 1],
            ['data' => $expected_summary],
            ['data' => $expected_sections],
          ],
        ],
      ],
    ]);

    $this->assertEquals($expected, $report->generateReport($values));
  }

}
