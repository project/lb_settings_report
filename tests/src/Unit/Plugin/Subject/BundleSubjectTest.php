<?php

namespace Drupal\Tests\lb_settings_report\Unit\Plugin\Subject;

use Drupal\Tests\UnitTestCase;
use Drupal\Tests\lb_settings_report\Unit\MockTestTrait;

/**
 * @coversDefaultClass \Drupal\lb_settings_report\Plugin\Subject\BundleSubject
 *
 * @group lb_settings_report
 */
class BundleSubjectTest extends UnitTestCase {

  use MockTestTrait;

  /**
   * Tests BundleSubject::getNumberedLabel().
   */
  public function testGetNumberedLabelDoesntShowOneIfIndexIsUnspecified() {
    $subject = $this->getLayoutBuilderSubjectMock();
    $this->assertEquals('Layout Builder Subject', $subject->getNumberedLabel());
  }

  /**
   * Tests BundleSubject::getNumberedLabel().
   */
  public function testGetNumberedLabelDoesntShowOneIfIndexIsZero() {
    $subject = $this->getLayoutBuilderSubjectMock(['idx' => 0]);
    $this->assertEquals('Layout Builder Subject', $subject->getNumberedLabel());
  }

  /**
   * Tests BundleSubject::getNumberedLabel().
   */
  public function testGetNumberedLabelShowsTwoIfIndexIsOne() {
    $subject = $this->getLayoutBuilderSubjectMock(['idx' => 1]);
    $this->assertEquals('Layout Builder Subject 2', $subject->getNumberedLabel());
  }

  /**
   * Tests BundleSubject::getFields().
   */
  public function testGetFieldsReturnsExpectedFields() {
    $subject = $this->getBundleSubjectMock([
      'idx' => 15,
      'uniqid' => 'abc123',
      'report' => 'some-report-id',
    ]);

    $expected = [
      '#title' => 'Layout Builder Subject 16',
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="report_type"]' => ['value' => 'some-report-id'],
        ],
      ],
      'entity_type' => [
        '#type' => 'select',
        '#title' => 'Subject entity type',
        '#options' => $this->getEntityTargetTypesMock(),
        '#validated' => TRUE,
        '#ajax' => [
          'callback' => [$subject, 'updateEntityType'],
          'wrapper' => "edit-abc123-bundle-wrapper",
        ],
      ],
      'entity_bundle' => [
        '#type' => 'select',
        '#title' => 'Subject entity bundle',
        '#options' => [],
        '#prefix' => "<div id='edit-abc123-bundle-wrapper'>",
        '#suffix' => '</div>',
        '#validated' => TRUE,
        '#states' => [
          'visible' => [
            ":input[name='abc123[entity_type]']" => ['empty' => FALSE],
          ],
        ],
      ],
      'entity_status' => [
        '#type' => 'select',
        '#title' => 'Status',
        '#options' => [
          '' => '- Any -',
          'published' => 'Published',
          'unpublished' => 'Unpublished',
        ],
        '#default_value' => 'published',
      ],
      'result_limit' => [
        '#type' => 'select',
        '#title' => 'Limit',
        '#options' => [
          '' => '- No Limit -',
          '5' => '5',
          '10' => '10',
          '25' => '25',
          '50' => '50',
          '100' => '100',
          '250' => '250',
          '500' => '500',
          '1000' => '1000',
        ],
        '#default_value' => 50,
      ],
    ];

    $this->assertEquals($expected, $subject->getFields());
  }

}
