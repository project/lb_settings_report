<?php

namespace Drupal\Tests\lb_settings_report\Unit\Plugin\Subject;

use Drupal\Tests\UnitTestCase;
use Drupal\Tests\lb_settings_report\Unit\MockTestTrait;

/**
 * @coversDefaultClass \Drupal\lb_settings_report\Plugin\Subject\LayoutBuilderSubject
 *
 * @group lb_settings_report
 */
class LayoutBuilderSubjectTest extends UnitTestCase {

  use MockTestTrait;

  /**
   * Tests LayoutBuilderSubject::getNumberedLabel().
   */
  public function testGetNumberedLabelDoesntShowOneIfIndexIsUnspecified() {
    $subject = $this->getLayoutBuilderSubjectMock();
    $this->assertEquals('Layout Builder Subject', $subject->getNumberedLabel());
  }

  /**
   * Tests LayoutBuilderSubject::getNumberedLabel().
   */
  public function testGetNumberedLabelDoesntShowOneIfIndexIsZero() {
    $subject = $this->getLayoutBuilderSubjectMock(['idx' => 0]);
    $this->assertEquals('Layout Builder Subject', $subject->getNumberedLabel());
  }

  /**
   * Tests LayoutBuilderSubject::getNumberedLabel().
   */
  public function testGetNumberedLabelShowsTwoIfIndexIsOne() {
    $subject = $this->getLayoutBuilderSubjectMock(['idx' => 1]);
    $this->assertEquals('Layout Builder Subject 2', $subject->getNumberedLabel());
  }

  /**
   * Tests LayoutBuilderSubject::getDefaultTargetType().
   */
  public function testGetDefaultTargetTypePopsItemOffList() {
    $subject = $this->getLayoutBuilderSubjectMock();
    $this->assertEquals('node', $subject->getDefaultTargetType());
  }

  /**
   * Tests LayoutBuilderSubject::getDefaultTargetType().
   */
  public function testGetDefaultTargetTypeShowsUserIfListIsEmpty() {
    $subject = $this->getLayoutBuilderSubjectMock([], [], TRUE);
    $this->assertEquals('user', $subject->getDefaultTargetType());
  }

  /**
   * Tests LayoutBuilderSubject::getFields().
   */
  public function testGetFieldsReturnsExpectedFields() {
    $subject = $this->getLayoutBuilderSubjectMock([
      'idx' => 15,
      'uniqid' => 'abc123',
      'report' => 'some-report-id',
    ]);

    $expected = [
      '#title' => 'Layout Builder Subject 16',
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="report_type"]' => ['value' => 'some-report-id'],
        ],
      ],
      'subject_type' => [
        '#title' => 'Subject type',
        '#type' => 'select',
        '#options' => [
          '' => '- Select a subject type -',
          'entity' => 'Entity override',
          'entity_type' => 'Entity type display',
        ],
      ],
      'entity_type_display' => [
        '#type' => 'select',
        '#title' => 'Entity type display',
        '#options' => $this->getEntityViewDisplayOptionsMock(),
        '#states' => [
          'visible' => [
            ":input[name='abc123[subject_type]']" => ['value' => 'entity_type'],
          ],
        ],
      ],
      'entity_type' => [
        '#type' => 'select',
        '#title' => 'Subject entity type',
        '#options' => $this->getEntityTargetTypesMock(),
        '#validated' => TRUE,
        '#ajax' => [
          'callback' => [$subject, 'updateEntityType'],
          'wrapper' => "edit-abc123-entity-wrapper",
        ],
        '#states' => [
          'visible' => [
            ":input[name='abc123[subject_type]']" => ['value' => 'entity'],
          ],
        ],
      ],
      'entity_id' => [
        '#type' => 'entity_autocomplete',
        '#title' => 'Subject entity',
        '#target_type' => 'node',
        '#selection_handler' => 'lb_settings_report:lb_override',
        '#selection_settings' => ['target_type' => 'node'],
        '#prefix' => "<div id='edit-abc123-entity-wrapper'>",
        '#suffix' => '</div>',
        '#states' => [
          'visible' => [
            ":input[name='abc123[entity_type]']" => ['empty' => FALSE],
          ],
        ],
      ],
    ];

    $this->assertEquals($expected, $subject->getFields());
  }

}
