<?php

namespace Drupal\Tests\lb_settings_report\Unit;

use Drupal\Core\Diff\DiffFormatter;
use Drupal\Core\Render\RendererInterface;
use Drupal\lb_settings_report\Plugin\Report\DiffReport;
use Drupal\lb_settings_report\Plugin\Report\OverrideReport;
use Drupal\lb_settings_report\Plugin\Report\SingleReport;
use Drupal\lb_settings_report\Plugin\Subject\BundleSubject;
use Drupal\lb_settings_report\Plugin\Subject\LayoutBuilderSubject;
use Drupal\lb_settings_report\Plugin\Subject\SubjectManager;

/**
 * Helper trait for unit tests that provides consistent mocking functionality.
 */
trait MockTestTrait {

  /**
   * Retrieves a mock entity view display options list.
   *
   * @return array
   *   Mock list.
   */
  public function getEntityViewDisplayOptionsMock() {
    return [
      'block_content.table.default' => 'Table (block_content.table.default)',
      'node.article.default' => 'Article (node.article.default)',
      'node.page.default' => 'Page (node.page.default)',
      'node.page.alt' => 'Page (node.page.alt)',
    ];
  }

  /**
   * Retrieves a mock entity target types list.
   *
   * @return array
   *   Mock list.
   */
  public function getEntityTargetTypesMock() {
    return [
      '' => '- Select a target type -',
      'block_content' => 'block_content',
      'node' => 'node',
    ];
  }

  /**
   * Helper method to retrieve a subject mock via class.
   *
   * @param string $class
   *   The class to mock.
   * @param array $config
   *   Optional configuration array to pass into the object.
   * @param array $mock_report
   *   Array containing mock report results.
   * @param bool $empty_target_types
   *   Indicates whether or not the target type list is empty.
   *
   * @return \Drupal\lb_settings_report\Plugin\Subject\LayoutBuilderSubject
   *   Subject object.
   */
  private function getSubjectMockByClass($class, array $config = [], array $mock_report = [], $empty_target_types = FALSE) {
    $subject = $this->getMockBuilder($class)
      ->disableOriginalConstructor()
      ->setMethods([
        'getPluginId',
        'label',
        'description',
        'getEntityViewDisplayOptions',
        'getEntityTargetTypes',
        'generateReport',
      ])
      ->getMock();
    $subject->expects($this->any())
      ->method('getPluginId')
      ->willReturn('lb_settings_report:layout_builder');
    $subject->expects($this->any())
      ->method('label')
      ->willReturn('Layout Builder Subject');
    $subject->expects($this->any())
      ->method('description')
      ->willReturn('Subject is an entity or entity type that uses layout builder.');
    $subject->expects($this->any())
      ->method('getEntityViewDisplayOptions')
      ->willReturn($this->getEntityViewDisplayOptionsMock());
    if ($empty_target_types) {
      $subject->expects($this->any())
        ->method('getEntityTargetTypes')
        ->willReturn(['' => 'Only contains an empty key']);
    }
    else {
      $subject->expects($this->any())
        ->method('getEntityTargetTypes')
        ->willReturn($this->getEntityTargetTypesMock());
    }
    $subject->expects($this->any())
      ->method('generateReport')
      ->willReturn($mock_report);
    $subject->setStringTranslation($this->getStringTranslationStub());
    $subject->setConfiguration($config);
    return $subject;
  }

  /**
   * Helper function to retrieve a mock subject object.
   *
   * @param array $config
   *   Optional configuration array to pass into the object.
   * @param array $mock_report
   *   Array containing mock report results.
   * @param bool $empty_target_types
   *   Indicates whether or not the target type list is empty.
   *
   * @return \Drupal\lb_settings_report\Plugin\Subject\LayoutBuilderSubject
   *   Subject object.
   */
  public function getLayoutBuilderSubjectMock(array $config = [], array $mock_report = [], $empty_target_types = FALSE) {
    return $this->getSubjectMockByClass(LayoutBuilderSubject::class, $config, $mock_report, $empty_target_types);
  }

  /**
   * Helper function to retrieve a mock subject object.
   *
   * @param array $config
   *   Optional configuration array to pass into the object.
   * @param array $mock_report
   *   Array containing mock report results.
   * @param bool $empty_target_types
   *   Indicates whether or not the target type list is empty.
   *
   * @return \Drupal\lb_settings_report\Plugin\Subject\BundleSubject
   *   Subject object.
   */
  public function getBundleSubjectMock(array $config = [], array $mock_report = [], $empty_target_types = FALSE) {
    return $this->getSubjectMockByClass(BundleSubject::class, $config, $mock_report, $empty_target_types);
  }

  /**
   * Helper function to retrieve a mock report based on the specified class.
   *
   * @param string $subject_class
   *   Class string.
   * @param int $index
   *   Index of the subject.
   */
  private function getMockReportBySubjectClass($subject_class, $index) {
    if ($subject_class === BundleSubject::class) {
      return [
        [
          'bundle' => 'block_content',
          'id' => '12345',
          'language' => 'Spanish',
          'published' => 'Published',
          'sections' => ['Section 1', 'Section 2', 'Section 3'],
          'title' => "Hola mundo {$index}",
        ],
      ];
    }
    return [
      'Mock Report',
      "Subject {$index}",
      'End of Report',
    ];
  }

  /**
   * Helper function to retrieve a mock subject manager object.
   *
   * @param int $subject_ct
   *   Number of subjects to generate.
   * @param string $subject_class
   *   Class of subject to generate.
   *
   * @return \Drupal\lb_settings_report\Plugin\Subject\SubjectManager
   *   Subject manager object.
   */
  public function getSubjectManagerMock($subject_ct, $subject_class) {
    $manager = $this->getMockBuilder(SubjectManager::class)
      ->disableOriginalConstructor()
      ->setMethods(['createInstance'])
      ->getMock();
    for ($i = 0; $i < $subject_ct; $i++) {
      $config = [
        'idx' => $i,
        'uniqid' => 'abc123',
        'report' => 'some-report-id',
      ];
      $mock_report = $this->getMockReportBySubjectClass($subject_class, $i);
      $manager->expects($this->at($i))
        ->method('createInstance')
        ->willReturn($this->getSubjectMockByClass($subject_class, $config, $mock_report));
    }

    return $manager;
  }

  /**
   * Helper function to retrieve a mock diff report object.
   *
   * @param int $subject_ct
   *   Number of subjects to generate.
   *
   * @return \Drupal\lb_settings_report\Plugin\Report\DiffReport
   *   Diff report object.
   */
  public function getDiffReportMock($subject_ct = 0) {
    $report = $this->getMockBuilder(DiffReport::class)
      ->disableOriginalConstructor()
      ->setMethods([
        'getPluginId',
        'label',
        'description',
      ])
      ->getMock();
    $report->expects($this->any())
      ->method('getPluginId')
      ->willReturn('lb_settings_report:diff');
    $report->expects($this->any())
      ->method('label')
      ->willReturn('Differential');
    $report->expects($this->any())
      ->method('description')
      ->willReturn('Provides a differential report for two different entities or entity types.');
    $report->setSubjectManager($this->getSubjectManagerMock($subject_ct, LayoutBuilderSubject::class));
    $report->setDiffFormatter($this->getDiffFormatterMock());
    $report->setRenderer($this->getRendererMock());
    $report->setStringTranslation($this->getStringTranslationStub());
    return $report;
  }

  /**
   * Helper function to retrieve a mock single report object.
   *
   * @param int $subject_ct
   *   Number of subjects to generate.
   *
   * @return \Drupal\lb_settings_report\Plugin\Report\SingleReport
   *   Diff report object.
   */
  public function getSingleReportMock($subject_ct = 0) {
    $report = $this->getMockBuilder(SingleReport::class)
      ->disableOriginalConstructor()
      ->setMethods([
        'getPluginId',
        'label',
        'description',
      ])
      ->getMock();
    $report->expects($this->any())
      ->method('getPluginId')
      ->willReturn('lb_settings_report:single');
    $report->expects($this->any())
      ->method('label')
      ->willReturn('Single');
    $report->expects($this->any())
      ->method('description')
      ->willReturn('Provides a layout builder settings report for a single entity or entity type.');
    $report->setSubjectManager($this->getSubjectManagerMock($subject_ct, LayoutBuilderSubject::class));
    $report->setStringTranslation($this->getStringTranslationStub());
    return $report;
  }

  /**
   * Helper function to retrieve a mock override report object.
   *
   * @param int $subject_ct
   *   Number of subjects to generate.
   *
   * @return \Drupal\lb_settings_report\Plugin\Report\SingleReport
   *   Diff report object.
   */
  public function getOverrideReportMock($subject_ct = 0) {
    $report = $this->getMockBuilder(OverrideReport::class)
      ->disableOriginalConstructor()
      ->setMethods([
        'getPluginId',
        'label',
        'description',
      ])
      ->getMock();
    $report->expects($this->any())
      ->method('getPluginId')
      ->willReturn('lb_settings_report:override');
    $report->expects($this->any())
      ->method('label')
      ->willReturn('Override');
    $report->expects($this->any())
      ->method('description')
      ->willReturn('Provides a layout builder settings report containing lists of overridden nodes for a bundle.');
    $report->setSubjectManager($this->getSubjectManagerMock($subject_ct, BundleSubject::class));
    $report->setRenderer($this->getRendererMock());
    $report->setStringTranslation($this->getStringTranslationStub());
    return $report;
  }

  /**
   * Helper function to retrieve a mock diff formatter.
   *
   * @return \Drupal\Core\Diff\DiffFormatter
   *   Diff formatter.
   */
  public function getDiffFormatterMock() {
    $diff_formatter = $this->getMockBuilder(DiffFormatter::class)
      ->disableOriginalConstructor()
      ->getMock();
    $diff_formatter->expects($this->any())
      ->method('format')
      ->will($this->returnCallback(function ($diff) {
        return 'Diff Results Go Here';
      }));
    return $diff_formatter;
  }

  /**
   * Helper function to retrieve a mock renderer.
   *
   * @return \Drupal\Core\Render\RendererInterface
   *   Renderer.
   */
  public function getRendererMock() {
    $renderer = $this->getMockBuilder(RendererInterface::class)
      ->getMock();
    $renderer->expects($this->any())
      ->method('render')
      ->will($this->returnCallback(function ($build) {
        return json_encode($build);
      }));
    return $renderer;
  }

}
