<?php

namespace Drupal\lb_settings_report\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\lb_settings_report\Plugin\Report\ReportManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for generating layout builder settings reports.
 */
class LayoutBuilderSettingsReportForm extends FormBase {

  /**
   * The report manager.
   *
   * @var \Drupal\lb_settings_report\Plugin\Report\ReportManager
   */
  protected $reportManager;

  /**
   * Constructs a new ConfigSingleImportForm.
   *
   * @param \Drupal\lb_settings_report\Plugin\Report\ReportManager $report_manager
   *   The report manager service.
   */
  public function __construct(ReportManager $report_manager) {
    $this->reportManager = $report_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.lb_settings_report.report')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'lb_settings_report';
  }

  /**
   * Retrieves list of report types.
   *
   * @return array
   *   List of report types.
   */
  public function getReportTypes() {
    return $this->reportManager->getDefinitions();
  }

  /**
   * Retrieves a list of options for the type dropdown.
   *
   * @return array
   *   List of options containing valid report types.
   */
  protected function getReportTypeOptions() {
    return [
      '' => $this->t('- Select a report type -'),
    ] + array_map(function ($type) {
      return $type['label'];
    }, $this->getReportTypes());
  }

  /**
   * Provides HTML containing report type definitions.
   */
  protected function getReportTypeDescriptionMarkup() {
    $html = '<dl>';
    $types = $this->getReportTypes();
    array_walk($types, function ($type) use (&$html) {
      $html .= '<dt>' . $type['label'] . '</dt>';
      $html .= '<dd>' . $type['description'] . '</dt>';
    });
    $html .= '</dl>';
    return $html;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $config_type = NULL, $config_name = NULL) {
    // Add style.
    $form['#attached']['library'][] = 'lb_settings_report/report';

    // Add report type field.
    $form['report_type'] = [
      '#title' => $this->t('Report type:'),
      '#description' => $this->getReportTypeDescriptionMarkup(),
      '#type' => 'select',
      '#options' => $this->getReportTypeOptions(),
    ];

    // Add subject fields to the form.
    foreach ($this->reportManager->getDefinitions() as $id => $report) {
      $this->reports[$id] = $this->reportManager->createInstance($id);
      foreach ($this->reports[$id]->getSubjects() as $uniqid => $subject) {
        $form[$uniqid] = $subject->getFields();
      }
    }

    // Add generate button.
    // TODO: Dynamically evaluate state of fields to enable/disable button.
    $form['submit'] = [
      '#type' => 'button',
      '#value' => $this->t('Generate Report'),
      '#ajax' => [
        'callback' => '::generateReport',
      ],
    ];

    // Add results region.
    $form['report'] = [
      '#type' => 'markup',
      '#markup' => '<div class="lbSettingsReport__message" id="lb-settings-report-results"></div>',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // TODO: Build ajax-less fallback.
  }

  /**
   * Attempts to generate the report.
   */
  public function generateReport($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $report_type = $values['report_type'];
    try {
      $report = $this->reports[$report_type];
      $html = $report->generateReport($values);
    }
    catch (\Exception $e) {
      $html = $this->t('Could not generate report. Please check settings and try again.');
    }

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand('.lbSettingsReport__message', $html));
    return $response;
  }

}
