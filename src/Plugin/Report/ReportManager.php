<?php

namespace Drupal\lb_settings_report\Plugin\Report;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the layout builder settings report plugin manager.
 */
class ReportManager extends DefaultPluginManager {

  /**
   * Constructs a new LayoutBuilderSettingsReportManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/Report', $namespaces, $module_handler, 'Drupal\lb_settings_report\Plugin\Report\ReportInterface', 'Drupal\lb_settings_report\Annotation\LayoutBuilderSettingsReport');

    $this->alterInfo('lb_settings_report_info');
    $this->setCacheBackend($cache_backend, 'lb_settings_report_info_plugins');
  }

}
