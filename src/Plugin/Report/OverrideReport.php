<?php

namespace Drupal\lb_settings_report\Plugin\Report;

/**
 * Report plugging for finding the difference between two subjects.
 *
 * @LayoutBuilderSettingsReport(
 *   id = "lb_settings_report:override",
 *   label = @Translation("Override", context = "lb_settings_report"),
 *   description = @Translation("Provides a layout builder settings report containing lists of overridden nodes for a bundle.", context = "lb_settings_report")
 * )
 */
class OverrideReport extends ReportBase {

  /**
   * {@inheritdoc}
   */
  public function generateReport(array $values) {
    $subjects = [];
    $subject_ct = 0;
    foreach ($this->getSubjects() as $subject_id => $subject) {
      if (!isset($values[$subject_id])) {
        throw new \Exception('Invalid subject id.');
      }

      $subjects[$subject_ct++] = $subject->generateReport($values[$subject_id]);
    }
    if ($subject_ct !== 1) {
      throw new \Exception('Invalid subject count');
    }

    $rows = [];
    $row_ct = 0;

    foreach ($subjects[0] as $row_details) {
      // If there is a valid link use that for our title, otherwise use the
      // title passed down to us.
      $title = $row_details['title'];
      if (isset($row_details['link'])) {
        $title = $row_details['link']->toRenderable();
        $title['#attributes']['target'] = '_blank';
        $title['#attributes']['rel'] = 'noopener noreferrer';
      }

      $section_ct = count($row_details['sections']);
      $summary_build = [
        '#theme' => 'lb_settings_report_override_summary',
        '#bundle' => $row_details['bundle'],
        '#id' => $row_details['id'],
        '#language' => $row_details['language'],
        '#published' => $row_details['published'] ? $this->t('Published') : $this->t('Unpublished'),
        '#title' => $title,
      ];
      $summary = $this->getRenderer()->render($summary_build);
      $sections_build = [
        '#theme' => 'lb_settings_report_override_sections',
        '#sections' => $row_details['sections'],
      ];
      $sections = $this->getRenderer()->render($sections_build);

      $rows[] = [
        ['data' => ++$row_ct],
        ['data' => $summary],
        ['data' => $sections],
      ];
    }

    $build = [];
    $build['#title'] = $this->t('Override Report');
    $build['overrides'] = [
      '#type' => 'table',
      '#attributes' => [
        'class' => [
          'lbSettingsReport__table',
          'lbSettingsReport__overrideTable',
        ],
      ],
      '#header' => [
        ['data' => ''],
        ['data' => $this->t('Entity Information')],
        ['data' => $this->t('Section Information')],
      ],
      '#rows' => $rows,
    ];

    return $this->getRenderer()->render($build);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubjects() {
    return $this->generateSubjectsByCount(1, 'lb_settings_report:bundle');
  }

}
