<?php

namespace Drupal\lb_settings_report\Plugin\Report;

/**
 * Report plugging for finding the difference between two subjects.
 *
 * @LayoutBuilderSettingsReport(
 *   id = "lb_settings_report:single",
 *   label = @Translation("Single", context = "lb_settings_report"),
 *   description = @Translation("Provides a layout builder settings report for a single entity or entity type.", context = "lb_settings_report")
 * )
 */
class SingleReport extends ReportBase {

  /**
   * {@inheritdoc}
   */
  public function generateReport(array $values) {
    $subjects = [];
    $subject_ct = 0;
    foreach ($this->getSubjects() as $subject_id => $subject) {
      if (!isset($values[$subject_id])) {
        throw new \Exception('Invalid subject id.');
      }

      $subjects[$subject_ct++] = $subject->generateReport($values[$subject_id]);
    }
    if ($subject_ct !== 1) {
      throw new \Exception('Invalid subject count');
    }

    return '<pre class="lbSettingsReport__preformatted">' . $this->encode($subjects[0]) . '</pre>';
  }

  /**
   * {@inheritdoc}
   */
  public function getSubjects() {
    return $this->generateSubjectsByCount(1, 'lb_settings_report:layout_builder');
  }

}
