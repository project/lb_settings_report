<?php

namespace Drupal\lb_settings_report\Plugin\Report;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Plugin\PluginBase;
use Drupal\lb_settings_report\Plugin\Subject\SubjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Capture utility plugins.
 */
abstract class ReportBase extends PluginBase implements ReportInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Subject Manager.
   *
   * @var \Drupal\lb_settings_report\Plugin\Subject\SubjectManager
   */
  protected $subjectManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SubjectManager $subject_manager, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
    $this->setSubjectManager($subject_manager);
    $this->setRenderer($renderer);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.lb_settings_report.subject'),
      $container->get('renderer')
    );
  }

  /**
   * Helper function for setting the subject manager.
   */
  public function setSubjectManager(SubjectManager $subject_manager) {
    $this->subjectManager = $subject_manager;
    return $this;
  }

  /**
   * Helper function to retrieve the subject manager.
   */
  public function getSubjectManager() {
    return $this->subjectManager;
  }

  /**
   * Helper function for setting the renderer.
   */
  public function setRenderer(RendererInterface $renderer) {
    $this->renderer = $renderer;
    return $this;
  }

  /**
   * Helper function to retrieve the renderer.
   */
  public function getRenderer() {
    return $this->renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function description() {
    return $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'id' => $this->getPluginId(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

  /**
   * Generates a list containing $count subjects of specified plugin ID.
   *
   * @param string[] $plugin_ids
   *   Array of plugin IDs to create instances of.
   *
   * @return \Drupal\lb_settings_report\Plugin\SubjectInterface[]
   *   Array of subjects.
   */
  protected function generateSubjectsByArray(array $plugin_ids) {
    $subjects = [];
    $count = 0;
    foreach ($plugin_ids as $plugin_id) {
      $uniqid = implode('__', [
        $this->getBaseId(),
        $this->getDerivativeId(),
        $count,
      ]);
      $configuration = [
        'idx' => $count++,
        'report' => $this->getPluginId(),
        'uniqid' => $uniqid,
      ];
      $subjects[$uniqid] = $this->getSubjectManager()->createInstance($plugin_id, $configuration);
    }
    return $subjects;
  }

  /**
   * Generates a list containing $count subjects of specified plugin ID.
   *
   * @param int $count
   *   Number of items plugins to create.
   * @param string $plugin_id
   *   Plugin ID of instance to create.
   *
   * @return \Drupal\lb_settings_report\Plugin\SubjectInterface[]
   *   Array of subjects.
   */
  protected function generateSubjectsByCount($count, $plugin_id) {
    return $this->generateSubjectsByArray(array_pad([], $count, $plugin_id));
  }

  /**
   * Encodes a subject value array into YAML.
   *
   * @param array $values
   *   Values array.
   *
   * @return string
   *   YAML string representing subject.
   */
  public function encode(array $values) {
    return trim(Yaml::encode($values));
  }

}
