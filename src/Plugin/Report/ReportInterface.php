<?php

namespace Drupal\lb_settings_report\Plugin\Report;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Defines an interface for layout builder settings report plugins.
 */
interface ReportInterface extends PluginInspectionInterface, ConfigurableInterface, DependentPluginInterface, ContainerFactoryPluginInterface {

  /**
   * Generates a report based on the specified values array.
   *
   * @param array $values
   *   Values array (usually populated via $form_state->getValues()).
   *
   * @return string
   *   HTML string representing the results of the report.
   */
  public function generateReport(array $values);

  /**
   * Retrieves a list of subjects for a particular report.
   *
   * @return \Drupal\lb_settings_report\Subject\SubjectInterface[]
   *   Array of subjects.
   */
  public function getSubjects();

}
