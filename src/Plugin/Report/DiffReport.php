<?php

namespace Drupal\lb_settings_report\Plugin\Report;

use Drupal\Component\Diff\Diff;
use Drupal\Core\Diff\DiffFormatter;
use Drupal\Core\Render\RendererInterface;
use Drupal\lb_settings_report\Plugin\Subject\SubjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Report plugging for finding the difference between two subjects.
 *
 * @LayoutBuilderSettingsReport(
 *   id = "lb_settings_report:diff",
 *   label = @Translation("Differential", context = "lb_settings_report"),
 *   description = @Translation("Provides a differential report for two different entities or entity types.", context = "lb_settings_report")
 * )
 */
class DiffReport extends ReportBase {

  /**
   * Diff formatter service.
   *
   * @var \Drupal\Core\Diff\DiffFormatter
   */
  protected $diffFormatter;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SubjectManager $subject_manager, RendererInterface $renderer, DiffFormatter $diff_formatter) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $subject_manager, $renderer);

    $this->setDiffFormatter($diff_formatter);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.lb_settings_report.subject'),
      $container->get('renderer'),
      $container->get('diff.formatter')
    );
  }

  /**
   * Helper function for setting the diff formatter.
   */
  public function setDiffFormatter(DiffFormatter $diff_formatter) {
    $this->diffFormatter = $diff_formatter;
    return $this;
  }

  /**
   * Helper function to retrieve the diff formatter.
   */
  public function getDiffFormatter() {
    return $this->diffFormatter;
  }

  /**
   * Retrieves a diff of two subjects.
   *
   * @param array $values
   *   Values array.
   *
   * @return \Drupal\Component\Diff\Diff
   *   The diff.
   */
  public function getDiff(array $values) {
    $subjects = [];
    $subject_ct = 0;
    foreach ($this->getSubjects() as $subject_id => $subject) {
      if (!isset($values[$subject_id])) {
        throw new \Exception('Invalid subject id.');
      }

      $subjects[$subject_ct++] = $subject->generateReport($values[$subject_id]);
    }
    if ($subject_ct !== 2) {
      throw new \Exception('Invalid subject count');
    }

    $left = explode(PHP_EOL, $this->encode($subjects[0]));
    $right = explode(PHP_EOL, $this->encode($subjects[1]));
    return new Diff($left, $right);
  }

  /**
   * {@inheritdoc}
   */
  public function generateReport(array $values) {
    $diff = $this->getDiff($values);

    // Build diff table.
    $build = [];
    $build['#title'] = $this->t('Differential Report');
    $build['#attached']['library'][] = 'system/diff';
    $build['diff'] = [
      '#type' => 'table',
      '#attributes' => [
        'class' => [
          'diff',
          'lbSettingsReport__table',
          'lbSettingsReport__diffTable',
        ],
      ],
      '#header' => [
        ['data' => $this->t('Subject 1'), 'colspan' => '2'],
        ['data' => $this->t('Subject 2'), 'colspan' => '2'],
      ],
      '#rows' => $this->getDiffFormatter()->format($diff),
    ];

    return $this->getRenderer()->render($build);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubjects() {
    if (!isset($this->subjects)) {
      $this->subjects = $this->generateSubjectsByCount(2, 'lb_settings_report:layout_builder');
    }
    return $this->subjects;
  }

}
