<?php

namespace Drupal\lb_settings_report\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;

/**
 * Limits results to entities that have layout builder overrides.
 *
 * @EntityReferenceSelection(
 *   id = "lb_settings_report:lb_override",
 *   label = @Translation("Layout builder enabled selection"),
 *   group = "lb_settings_report",
 *   weight = 1
 * )
 */
class LayoutBuilderOverrideSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);
    $query->exists(OverridesSectionStorage::FIELD_NAME);
    return $query;
  }

}
