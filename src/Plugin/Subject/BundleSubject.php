<?php

namespace Drupal\lb_settings_report\Plugin\Subject;

use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;

/**
 * Plugin for entity bundle subjects.
 *
 * @LayoutBuilderSettingsReportSubject(
 *   id = "lb_settings_report:bundle",
 *   label = @Translation("Entity Bundle Subject", context = "lb_settings_report"),
 *   description = @Translation("Subject is an entity or entity type that uses layout builder.", context = "lb_settings_report")
 * )
 */
class BundleSubject extends SubjectBase {

  /**
   * {@inheritdoc}
   */
  public function generateReport(array $values) {
    $report = [];
    $storage = $this->entityTypeManager->getStorage($values['entity_type']);
    $query = $storage->getQuery();
    $status_list = [];

    if (!empty($values['entity_bundle'])) {
      $query = $query->condition('type', $values['entity_bundle']);
    }
    if (empty($values['entity_status']) || $values['entity_status'] === 'published') {
      $status_list[] = 1;
    }
    if (empty($values['entity_status']) || $values['entity_status'] === 'unpublished') {
      $status_list[] = 0;
    }
    $query = $query
      ->condition('status', $status_list, 'IN')
      ->exists(OverridesSectionStorage::FIELD_NAME)
      ->range(0, $values['result_limit']);
    $ids = $query->execute();
    $chunk_size = 50;
    $chunks = array_chunk($ids, $chunk_size, TRUE);
    foreach ($chunks as $chunk) {
      $entities = $storage->loadMultiple($chunk);
      foreach ($entities as $entity) {
        $sections = $entity->get(OverridesSectionStorage::FIELD_NAME)->getSections();
        $section_details = [];
        foreach ($sections as $section) {
          $settings = $section->getLayoutSettings();
          $section_details[] = [
            'label' => $settings['label'],
            'layout_id' => $section->getLayoutId(),
            'components' => count($section->getComponents()),
          ];
        }

        $report[$entity->id()] = [
          'bundle' => $entity->bundle(),
          'id' => $entity->id(),
          'language' => $entity->language()->getName(),
          'link' => $entity->toLink(),
          'published' => $entity->isPublished(),
          'sections' => $section_details,
          'title' => $entity->label(),
        ];
      }
    }
    return $report;
  }

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    $fields = parent::getFields();
    $config = $this->getConfiguration();
    $uniqid = $config['uniqid'];

    $fields['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Subject entity type'),
      '#options' => $this->getEntityTargetTypes(),
      '#validated' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'updateEntityType'],
        'wrapper' => "edit-{$uniqid}-bundle-wrapper",
      ],
    ];

    $fields['entity_bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Subject entity bundle'),
      '#options' => [],
      '#prefix' => "<div id='edit-{$uniqid}-bundle-wrapper'>",
      '#suffix' => '</div>',
      '#validated' => TRUE,
      '#states' => [
        'visible' => [
          ":input[name='{$uniqid}[entity_type]']" => ['empty' => FALSE],
        ],
      ],
    ];

    $fields['entity_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Status'),
      '#options' => [
        '' => $this->t('- Any -'),
        'published' => $this->t('Published'),
        'unpublished' => $this->t('Unpublished'),
      ],
      '#default_value' => 'published',
    ];

    $fields['result_limit'] = [
      '#type' => 'select',
      '#title' => $this->t('Limit'),
      '#options' => [
        '' => $this->t('- No Limit -'),
        '5' => '5',
        '10' => '10',
        '25' => '25',
        '50' => '50',
        '100' => '100',
        '250' => '250',
        '500' => '500',
        '1000' => '1000',
      ],
      '#default_value' => 50,
    ];

    return $fields;
  }

  /**
   * Handles ajax event when entity type field is updated.
   */
  public function updateEntityType($form, FormStateInterface $form_state) {
    $bundles = $this->getEntityBundles();
    $config = $this->getConfiguration();
    $uniqid = $config['uniqid'];
    $field = $form[$uniqid]['entity_bundle'];
    $target_type = $form_state->getValue($uniqid)['entity_type'];

    $field['#options'] = [
      '' => $this->t('- Any -'),
    ];

    if (!empty($target_type) && !empty($bundles[$target_type])) {
      $field['#options'] += $bundles[$target_type];
    }

    return $field;
  }

}
