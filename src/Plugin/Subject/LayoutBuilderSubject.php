<?php

namespace Drupal\lb_settings_report\Plugin\Subject;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;

/**
 * Plugin for layout-dbuilder-enabled subjects.
 *
 * @LayoutBuilderSettingsReportSubject(
 *   id = "lb_settings_report:layout_builder",
 *   label = @Translation("Layout Builder Subject", context = "lb_settings_report"),
 *   description = @Translation("Subject is an entity or entity type that uses layout builder.", context = "lb_settings_report")
 * )
 */
class LayoutBuilderSubject extends SubjectBase {

  /**
   * Generates the entity override report.
   */
  protected function generateEntityReport(array $values) {
    $report = [];
    $entity = $this->entityTypeManager->getStorage($values['entity_type'])->load($values['entity_id']);
    if ($entity && $entity->hasField(OverridesSectionStorage::FIELD_NAME)) {
      $overrides = $entity->get(OverridesSectionStorage::FIELD_NAME);
      $report['allow_custom'] = TRUE;
      $report['enabled'] = TRUE;
      $report['sections'] = [];
      foreach ($overrides->getSections() as $section) {
        $section_details = [
          'layout_id' => $section->getLayoutId(),
          'layout_settings' => $section->getLayoutSettings(),
        ];
        $components = $section->getComponents();
        if (!empty($components)) {
          $section_details['components'] = [];
          foreach ($components as $component) {
            $section_details['components'][$component->getUuid()] = $component->toArray();
          }
        }
        $report['sections'][] = $section_details;
      }
    }
    return $report;
  }

  /**
   * Generates the entity type view display report.
   */
  protected function generateEntityTypeReport(array $values) {
    $definition = $this->entityTypeManager->getDefinition('entity_view_display');
    $name = $definition->getConfigPrefix() . '.' . $values['entity_type_display'];
    $config = $this->configStorage->read($name);
    if (!empty($config['third_party_settings']['layout_builder'])) {
      return $config['third_party_settings']['layout_builder'];
    }

    throw new \Exception('Invalid entity type subject');
  }

  /**
   * {@inheritdoc}
   */
  public function generateReport(array $values) {
    if ($values['subject_type'] === 'entity') {
      return $this->generateEntityReport($values);
    }
    elseif ($values['subject_type'] === 'entity_type') {
      return $this->generateEntityTypeReport($values);
    }
    throw new \Exception('Invalid subject type specified');
  }

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    $fields = parent::getFields();
    $config = $this->getConfiguration();
    $uniqid = $config['uniqid'];
    $fields['subject_type'] = [
      '#title' => $this->t('Subject type'),
      '#type' => 'select',
      '#options' => [
        '' => $this->t('- Select a subject type -'),
        'entity' => $this->t('Entity override'),
        'entity_type' => $this->t('Entity type display'),
      ],
    ];
    $fields['entity_type_display'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity type display'),
      '#options' => $this->getEntityViewDisplayOptions(),
      '#states' => [
        'visible' => [
          ":input[name='{$uniqid}[subject_type]']" => ['value' => 'entity_type'],
        ],
      ],
    ];

    $fields['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Subject entity type'),
      '#options' => $this->getEntityTargetTypes(),
      '#validated' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'updateEntityType'],
        'wrapper' => "edit-{$uniqid}-entity-wrapper",
      ],
      '#states' => [
        'visible' => [
          ":input[name='{$uniqid}[subject_type]']" => ['value' => 'entity'],
        ],
      ],
    ];
    $fields['entity_id'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Subject entity'),
      '#target_type' => $this->getDefaultTargetType(),
      '#selection_handler' => 'lb_settings_report:lb_override',
      '#selection_settings' => ['target_type' => $this->getDefaultTargetType()],
      '#prefix' => "<div id='edit-{$uniqid}-entity-wrapper'>",
      '#suffix' => '</div>',
      '#states' => [
        'visible' => [
          ":input[name='{$uniqid}[entity_type]']" => ['empty' => FALSE],
        ],
      ],
    ];

    return $fields;
  }

  /**
   * Handles ajax event when entity type field is updated.
   */
  public function updateEntityType($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    $uniqid = $config['uniqid'];
    $field = $form[$uniqid]['entity_id'];
    $target_type = $form_state->getValue($uniqid)['entity_type'];
    if (!empty($target_type)) {
      $field['#target_type'] = $target_type;
      $field['#selection_settings']['target_type'] = $target_type;

      // Store the selection settings in the key/value store and pass a hashed
      // key in the route parameters.
      // @see \Drupal\Core\Entity\Element\EntityAutocomplete::processEntityAutocomplete()
      $selection_settings = isset($field['#selection_settings']) ? $field['#selection_settings'] : [];
      $data = serialize($selection_settings) . $field['#target_type'] . $field['#selection_handler'];
      $selection_settings_key = Crypt::hmacBase64($data, Settings::getHashSalt());

      $key_value_storage = \Drupal::keyValue('entity_autocomplete');
      $key_value_storage->set($selection_settings_key, $selection_settings);

      $field['#autocomplete_route_name'] = 'system.entity_autocomplete';
      $field['#autocomplete_route_parameters'] = [
        'target_type' => $field['#target_type'],
        'selection_handler' => $field['#selection_handler'],
        'selection_settings_key' => $selection_settings_key,
      ];

      $url = Url::fromRoute($field['#autocomplete_route_name'], $field['#autocomplete_route_parameters'])->toString(TRUE);
      $field['#attributes']['data-autocomplete-path'] = $url->getGeneratedUrl();
    }
    return $field;
  }

}
