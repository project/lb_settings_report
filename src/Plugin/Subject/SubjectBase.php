<?php

namespace Drupal\lb_settings_report\Plugin\Subject;

use Drupal\Core\Config\StorageInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Plugin\PluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Capture utility plugins.
 */
abstract class SubjectBase extends PluginBase implements SubjectInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * Entity bundle mapping.
   *
   * @var array
   */
  static protected $entityBundles;

  /**
   * List of entity view displays in the the system.
   *
   * @var array
   */
  static protected $entityViewDisplayOptions;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Config storage.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $configStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, StorageInterface $config_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->configStorage = $config_storage;

    // We only need to set this once to share across all subjects.
    if (!isset(self::$entityViewDisplayOptions)) {
      $this->initializeLayoutBuilderSettings();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('config.storage')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function description() {
    return $this->pluginDefinition['description'];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'id' => $this->getPluginId(),
      'idx' => 0,
      'report' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

  /**
   * Initializes layout builder display settings.
   */
  private function initializeLayoutBuilderSettings() {
    $entity_storage = $this->entityTypeManager->getStorage('entity_view_display');
    $definition = $this->entityTypeManager->getDefinition('entity_view_display');

    self::$entityViewDisplayOptions = [];

    $target_types = [];
    foreach ($entity_storage->loadMultiple() as $entity) {
      $entity_id = $entity->id();
      $name = $definition->getConfigPrefix() . '.' . $entity_id;
      $config = $this->configStorage->read($name);
      if (!empty($config['third_party_settings']['layout_builder'])) {
        $target_types[$config['targetEntityType']][$config['bundle']] = $config['bundle'];
        if ($label = $entity->label()) {
          self::$entityViewDisplayOptions[$entity_id] = $this->t('@label (@id)', [
            '@label' => $label,
            '@id' => $entity_id,
          ]);
        }
        else {
          self::$entityViewDisplayOptions[$entity_id] = $entity_id;
        }
      }
    }
    self::$entityBundles = $target_types;
  }

  /**
   * Helper method to retrieve entity view display options from static memory.
   */
  public function getEntityViewDisplayOptions() {
    return static::$entityViewDisplayOptions;
  }

  /**
   * Helper method to retrieve entity target types from static memory.
   */
  public function getEntityTargetTypes() {
    $type_keys = array_keys(static::$entityBundles);
    return [
      '' => $this->t('- Select entity type -'),
    ] + array_combine($type_keys, $type_keys);
  }

  /**
   * Helper method to retrieve entity bundles from static memory.
   */
  public function getEntityBundles() {
    return static::$entityBundles;
  }

  /**
   * Retrieves the default target type.
   *
   * @return string
   *   Default target type.
   */
  public function getDefaultTargetType() {
    $filtered = array_filter($this->getEntityTargetTypes(), function ($item) {
      return !empty($item);
    }, ARRAY_FILTER_USE_KEY);
    if (!empty($filtered)) {
      return array_pop($filtered);
    }

    // Pick a sensible default that is most likely to be installed on someone's
    // system. In this particular case, since the report is only ever accessible
    // via the admin, we should be able to assume the user system is enabled.
    return 'user';
  }

  /**
   * Subject title.
   */
  public function getNumberedLabel() {
    $count = $this->getConfiguration()['idx'] + 1;
    if ($count > 1) {
      return "{$this->label()} {$count}";
    }
    return $this->label();
  }

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    $config = $this->getConfiguration();
    $uniqid = $config['uniqid'];
    $fields = [
      '#title' => $this->getNumberedLabel(),
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="report_type"]' => ['value' => $config['report']],
        ],
      ],
    ];
    return $fields;
  }

}
