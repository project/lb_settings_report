<?php

namespace Drupal\lb_settings_report\Plugin\Subject;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Defines an interface for layout builder settings report plugins.
 */
interface SubjectInterface extends PluginInspectionInterface, ConfigurableInterface, DependentPluginInterface, ContainerFactoryPluginInterface {

  /**
   * Retrieves a report for an individual subject.
   *
   * @param array $values
   *   Array of values on which to report.
   *
   * @return string
   *   Subject report.
   */
  public function generateReport(array $values);

  /**
   * Retrieves subject fields for the form array.
   *
   * @return array
   *   Render array.
   */
  public function getFields();

}
