<?php

namespace Drupal\lb_settings_report\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a layout builder settings report subject annotation object.
 *
 * @see \Drupal\lb_settings_report\Plugin\Subject\SubjectManager
 * @see plugin_api
 *
 * @Annotation
 */
class LayoutBuilderSettingsReportSubject extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * A brief description of the capture utility.
   *
   * This will be shown when adding or configuring this capture utility.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description = '';

}
